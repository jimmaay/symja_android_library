package org.matheclipse.core.integrate.rubi45;


import static org.matheclipse.core.expression.F.*;
import static org.matheclipse.core.integrate.rubi45.UtilityFunctionCtors.*;
import static org.matheclipse.core.integrate.rubi45.UtilityFunctions.*;
import org.matheclipse.core.interfaces.IAST;
import org.matheclipse.core.interfaces.IExpr;
import org.matheclipse.core.interfaces.ISymbol;

/** 
 * IndefiniteIntegrationRules from the <a href="http://www.apmaths.uwo.ca/~arich/">Rubi -
 * rule-based integrator</a>.
 *  
 */
public class IntRules124 { 
  public static IAST RULES = List( 
ISetDelayed(Int(Times(ArcTanh(Plus(a_,Times(b_DEFAULT,x_))),Power(Plus(c_,Times($p(d,true),Power(x_,n_DEFAULT))),CN1)),x_Symbol),
    Condition(Plus(Times(C1D2,Int(Times(Log(Plus(C1,a,Times(b,x))),Power(Plus(c,Times(d,Power(x,n))),CN1)),x)),Times(CN1,C1D2,Int(Times(Log(Plus(C1,Times(CN1,a),Times(CN1,b,x))),Power(Plus(c,Times(d,Power(x,n))),CN1)),x))),And(FreeQ(List(a,b,c,d),x),RationalQ(n)))),
ISetDelayed(Int(Times(ArcCoth(Plus(a_,Times(b_DEFAULT,x_))),Power(Plus(c_,Times($p(d,true),Power(x_,n_DEFAULT))),CN1)),x_Symbol),
    Condition(Plus(Times(C1D2,Int(Times(Log(Times(Plus(C1,a,Times(b,x)),Power(Plus(a,Times(b,x)),CN1))),Power(Plus(c,Times(d,Power(x,n))),CN1)),x)),Times(CN1,C1D2,Int(Times(Log(Times(Plus(CN1,a,Times(b,x)),Power(Plus(a,Times(b,x)),CN1))),Power(Plus(c,Times(d,Power(x,n))),CN1)),x))),And(FreeQ(List(a,b,c,d),x),RationalQ(n)))),
ISetDelayed(Int(Times(ArcTanh(Plus(a_,Times(b_DEFAULT,x_))),Power(Plus(c_,Times($p(d,true),Power(x_,n_))),CN1)),x_Symbol),
    Condition($(Defer($s("Int")),Times(ArcTanh(Plus(a,Times(b,x))),Power(Plus(c,Times(d,Power(x,n))),CN1)),x),And(FreeQ(List(a,b,c,d,n),x),Not(RationalQ(n))))),
ISetDelayed(Int(Times(ArcCoth(Plus(a_,Times(b_DEFAULT,x_))),Power(Plus(c_,Times($p(d,true),Power(x_,n_))),CN1)),x_Symbol),
    Condition($(Defer($s("Int")),Times(ArcCoth(Plus(a,Times(b,x))),Power(Plus(c,Times(d,Power(x,n))),CN1)),x),And(FreeQ(List(a,b,c,d,n),x),Not(RationalQ(n))))),
ISetDelayed(Int(ArcTanh(Plus(a_,Times(b_DEFAULT,Power(x_,n_)))),x_Symbol),
    Condition(Plus(Times(x,ArcTanh(Plus(a,Times(b,Power(x,n))))),Times(CN1,b,n,Int(Times(Power(x,n),Power(Plus(C1,Times(CN1,Sqr(a)),Times(CN1,C2,a,b,Power(x,n)),Times(CN1,Sqr(b),Power(x,Times(C2,n)))),CN1)),x))),FreeQ(List(a,b,n),x))),
ISetDelayed(Int(ArcCoth(Plus(a_,Times(b_DEFAULT,Power(x_,n_)))),x_Symbol),
    Condition(Plus(Times(x,ArcCoth(Plus(a,Times(b,Power(x,n))))),Times(CN1,b,n,Int(Times(Power(x,n),Power(Plus(C1,Times(CN1,Sqr(a)),Times(CN1,C2,a,b,Power(x,n)),Times(CN1,Sqr(b),Power(x,Times(C2,n)))),CN1)),x))),FreeQ(List(a,b,n),x))),
ISetDelayed(Int(Times(ArcTanh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,n_DEFAULT)))),Power(x_,CN1)),x_Symbol),
    Condition(Plus(Times(C1D2,Int(Times(Log(Plus(C1,a,Times(b,Power(x,n)))),Power(x,CN1)),x)),Times(CN1,C1D2,Int(Times(Log(Plus(C1,Times(CN1,a),Times(CN1,b,Power(x,n)))),Power(x,CN1)),x))),FreeQ(List(a,b,n),x))),
ISetDelayed(Int(Times(ArcCoth(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,n_DEFAULT)))),Power(x_,CN1)),x_Symbol),
    Condition(Plus(Times(C1D2,Int(Times(Log(Plus(C1,Power(Plus(a,Times(b,Power(x,n))),CN1))),Power(x,CN1)),x)),Times(CN1,C1D2,Int(Times(Log(Plus(C1,Times(CN1,Power(Plus(a,Times(b,Power(x,n))),CN1)))),Power(x,CN1)),x))),FreeQ(List(a,b,n),x))),
ISetDelayed(Int(Times(ArcTanh(Plus(a_,Times(b_DEFAULT,Power(x_,n_)))),Power(x_,m_DEFAULT)),x_Symbol),
    Condition(Plus(Times(Power(x,Plus(m,C1)),ArcTanh(Plus(a,Times(b,Power(x,n)))),Power(Plus(m,C1),CN1)),Times(CN1,b,n,Power(Plus(m,C1),CN1),Int(Times(Power(x,Plus(m,n)),Power(Plus(C1,Times(CN1,Sqr(a)),Times(CN1,C2,a,b,Power(x,n)),Times(CN1,Sqr(b),Power(x,Times(C2,n)))),CN1)),x))),And(And(And(FreeQ(List(a,b),x),RationalQ(m,n)),Unequal(Plus(m,C1),C0)),Unequal(Plus(m,C1),n)))),
ISetDelayed(Int(Times(ArcCoth(Plus(a_,Times(b_DEFAULT,Power(x_,n_)))),Power(x_,m_DEFAULT)),x_Symbol),
    Condition(Plus(Times(Power(x,Plus(m,C1)),ArcCoth(Plus(a,Times(b,Power(x,n)))),Power(Plus(m,C1),CN1)),Times(CN1,b,n,Power(Plus(m,C1),CN1),Int(Times(Power(x,Plus(m,n)),Power(Plus(C1,Times(CN1,Sqr(a)),Times(CN1,C2,a,b,Power(x,n)),Times(CN1,Sqr(b),Power(x,Times(C2,n)))),CN1)),x))),And(And(And(FreeQ(List(a,b),x),RationalQ(m,n)),Unequal(Plus(m,C1),C0)),Unequal(Plus(m,C1),n)))),
ISetDelayed(Int(ArcTanh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(f_,Plus(c_DEFAULT,Times($p(d,true),x_)))))),x_Symbol),
    Condition(Plus(Times(C1D2,Int(Log(Plus(C1,a,Times(b,Power(f,Plus(c,Times(d,x)))))),x)),Times(CN1,C1D2,Int(Log(Plus(C1,Times(CN1,a),Times(CN1,b,Power(f,Plus(c,Times(d,x)))))),x))),FreeQ(List(a,b,c,d,f),x))),
ISetDelayed(Int(ArcCoth(Plus(a_DEFAULT,Times(b_DEFAULT,Power(f_,Plus(c_DEFAULT,Times($p(d,true),x_)))))),x_Symbol),
    Condition(Plus(Times(C1D2,Int(Log(Plus(C1,Power(Plus(a,Times(b,Power(f,Plus(c,Times(d,x))))),CN1))),x)),Times(CN1,C1D2,Int(Log(Plus(C1,Times(CN1,Power(Plus(a,Times(b,Power(f,Plus(c,Times(d,x))))),CN1)))),x))),FreeQ(List(a,b,c,d,f),x))),
ISetDelayed(Int(Times(ArcTanh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(f_,Plus(c_DEFAULT,Times($p(d,true),x_)))))),Power(x_,m_DEFAULT)),x_Symbol),
    Condition(Plus(Times(C1D2,Int(Times(Power(x,m),Log(Plus(C1,a,Times(b,Power(f,Plus(c,Times(d,x))))))),x)),Times(CN1,C1D2,Int(Times(Power(x,m),Log(Plus(C1,Times(CN1,a),Times(CN1,b,Power(f,Plus(c,Times(d,x))))))),x))),And(And(FreeQ(List(a,b,c,d,f),x),IntegerQ(m)),Greater(m,C0)))),
ISetDelayed(Int(Times(ArcCoth(Plus(a_DEFAULT,Times(b_DEFAULT,Power(f_,Plus(c_DEFAULT,Times($p(d,true),x_)))))),Power(x_,m_DEFAULT)),x_Symbol),
    Condition(Plus(Times(C1D2,Int(Times(Power(x,m),Log(Plus(C1,Power(Plus(a,Times(b,Power(f,Plus(c,Times(d,x))))),CN1)))),x)),Times(CN1,C1D2,Int(Times(Power(x,m),Log(Plus(C1,Times(CN1,Power(Plus(a,Times(b,Power(f,Plus(c,Times(d,x))))),CN1))))),x))),And(And(FreeQ(List(a,b,c,d,f),x),IntegerQ(m)),Greater(m,C0)))),
ISetDelayed(Int(Times(u_DEFAULT,Power(ArcTanh(Times(c_DEFAULT,Power(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,n_DEFAULT))),CN1))),m_DEFAULT)),x_Symbol),
    Condition(Int(Times(u,Power(ArcCoth(Plus(Times(a,Power(c,CN1)),Times(b,Power(x,n),Power(c,CN1)))),m)),x),FreeQ(List(a,b,c,n,m),x))),
ISetDelayed(Int(Times(u_DEFAULT,Power(ArcCoth(Times(c_DEFAULT,Power(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,n_DEFAULT))),CN1))),m_DEFAULT)),x_Symbol),
    Condition(Int(Times(u,Power(ArcTanh(Plus(Times(a,Power(c,CN1)),Times(b,Power(x,n),Power(c,CN1)))),m)),x),FreeQ(List(a,b,c,n,m),x))),
ISetDelayed(Int(Power(Times(ArcTanh(Times(c_DEFAULT,x_,Power(Plus(a_DEFAULT,Times(b_DEFAULT,Sqr(x_))),CN1D2))),Sqrt(Plus(a_DEFAULT,Times(b_DEFAULT,Sqr(x_))))),CN1),x_Symbol),
    Condition(Times(Power(c,CN1),Log(ArcTanh(Times(c,x,Power(Plus(a,Times(b,Sqr(x))),CN1D2))))),And(FreeQ(List(a,b,c),x),ZeroQ(Plus(b,Times(CN1,Sqr(c))))))),
ISetDelayed(Int(Power(Times(ArcCoth(Times(c_DEFAULT,x_,Power(Plus(a_DEFAULT,Times(b_DEFAULT,Sqr(x_))),CN1D2))),Sqrt(Plus(a_DEFAULT,Times(b_DEFAULT,Sqr(x_))))),CN1),x_Symbol),
    Condition(Times(CN1,Power(c,CN1),Log(ArcCoth(Times(c,x,Power(Plus(a,Times(b,Sqr(x))),CN1D2))))),And(FreeQ(List(a,b,c),x),ZeroQ(Plus(b,Times(CN1,Sqr(c))))))),
ISetDelayed(Int(Times(Power(ArcTanh(Times(c_DEFAULT,x_,Power(Plus(a_DEFAULT,Times(b_DEFAULT,Sqr(x_))),CN1D2))),m_DEFAULT),Power(Plus(a_DEFAULT,Times(b_DEFAULT,Sqr(x_))),CN1D2)),x_Symbol),
    Condition(Times(Power(ArcTanh(Times(c,x,Power(Plus(a,Times(b,Sqr(x))),CN1D2))),Plus(m,C1)),Power(Times(c,Plus(m,C1)),CN1)),And(And(FreeQ(List(a,b,c,m),x),ZeroQ(Plus(b,Times(CN1,Sqr(c))))),NonzeroQ(Plus(m,C1))))),
ISetDelayed(Int(Times(Power(ArcCoth(Times(c_DEFAULT,x_,Power(Plus(a_DEFAULT,Times(b_DEFAULT,Sqr(x_))),CN1D2))),m_DEFAULT),Power(Plus(a_DEFAULT,Times(b_DEFAULT,Sqr(x_))),CN1D2)),x_Symbol),
    Condition(Times(CN1,Power(ArcCoth(Times(c,x,Power(Plus(a,Times(b,Sqr(x))),CN1D2))),Plus(m,C1)),Power(Times(c,Plus(m,C1)),CN1)),And(And(FreeQ(List(a,b,c,m),x),ZeroQ(Plus(b,Times(CN1,Sqr(c))))),NonzeroQ(Plus(m,C1))))),
ISetDelayed(Int(Times(Power(ArcTanh(Times(c_DEFAULT,x_,Power(Plus(a_DEFAULT,Times(b_DEFAULT,Sqr(x_))),CN1D2))),m_DEFAULT),Power(Plus($p(d,true),Times($p(e,true),Sqr(x_))),CN1D2)),x_Symbol),
    Condition(Times(Sqrt(Plus(a,Times(b,Sqr(x)))),Power(Plus(d,Times(e,Sqr(x))),CN1D2),Int(Times(Power(ArcTanh(Times(c,x,Power(Plus(a,Times(b,Sqr(x))),CN1D2))),m),Power(Plus(a,Times(b,Sqr(x))),CN1D2)),x)),And(And(FreeQ(List(a,b,c,d,e,m),x),ZeroQ(Plus(b,Times(CN1,Sqr(c))))),ZeroQ(Plus(Times(b,d),Times(CN1,a,e)))))),
ISetDelayed(Int(Times(Power(ArcCoth(Times(c_DEFAULT,x_,Power(Plus(a_DEFAULT,Times(b_DEFAULT,Sqr(x_))),CN1D2))),m_DEFAULT),Power(Plus($p(d,true),Times($p(e,true),Sqr(x_))),CN1D2)),x_Symbol),
    Condition(Times(Sqrt(Plus(a,Times(b,Sqr(x)))),Power(Plus(d,Times(e,Sqr(x))),CN1D2),Int(Times(Power(ArcCoth(Times(c,x,Power(Plus(a,Times(b,Sqr(x))),CN1D2))),m),Power(Plus(a,Times(b,Sqr(x))),CN1D2)),x)),And(And(FreeQ(List(a,b,c,d,e,m),x),ZeroQ(Plus(b,Times(CN1,Sqr(c))))),ZeroQ(Plus(Times(b,d),Times(CN1,a,e)))))),
ISetDelayed(Int(Power(E,Times(n_DEFAULT,ArcTanh(Times(a_DEFAULT,x_)))),x_Symbol),
    Condition(Int(Times(Power(Plus(C1,Times(a,x)),Times(C1D2,Plus(n,C1))),Power(Times(Power(Plus(C1,Times(CN1,a,x)),Times(C1D2,Plus(n,Times(CN1,C1)))),Sqrt(Plus(C1,Times(CN1,Sqr(a),Sqr(x))))),CN1)),x),And(FreeQ(a,x),OddQ(n)))),
ISetDelayed(Int(Times(Power(E,Times(n_DEFAULT,ArcTanh(Times(a_DEFAULT,x_)))),Power(x_,m_DEFAULT)),x_Symbol),
    Condition(Int(Times(Power(x,m),Power(Plus(C1,Times(a,x)),Times(C1D2,Plus(n,C1))),Power(Times(Power(Plus(C1,Times(CN1,a,x)),Times(C1D2,Plus(n,Times(CN1,C1)))),Sqrt(Plus(C1,Times(CN1,Sqr(a),Sqr(x))))),CN1)),x),And(FreeQ(List(a,m),x),OddQ(n)))),
ISetDelayed(Int(Power(E,Times(n_,ArcTanh(Times(a_DEFAULT,x_)))),x_Symbol),
    Condition(Int(Times(Power(Plus(C1,Times(a,x)),Times(C1D2,n)),Power(Power(Plus(C1,Times(CN1,a,x)),Times(C1D2,n)),CN1)),x),And(FreeQ(List(a,n),x),Not(OddQ(n))))),
ISetDelayed(Int(Times(Power(E,Times(n_,ArcTanh(Times(a_DEFAULT,x_)))),Power(x_,m_DEFAULT)),x_Symbol),
    Condition(Int(Times(Power(x,m),Power(Plus(C1,Times(a,x)),Times(C1D2,n)),Power(Power(Plus(C1,Times(CN1,a,x)),Times(C1D2,n)),CN1)),x),And(FreeQ(List(a,m,n),x),Not(OddQ(n))))),
ISetDelayed(Int(Times(u_DEFAULT,Power(Plus(c_,Times($p(d,true),x_)),p_DEFAULT),Power(E,Times(n_DEFAULT,ArcTanh(Times(a_DEFAULT,x_))))),x_Symbol),
    Condition(Times(Power(c,p),Int(Times(u,Power(Plus(C1,Times(d,x,Power(c,CN1))),p),Power(Plus(C1,Times(a,x)),Times(C1D2,n)),Power(Power(Plus(C1,Times(CN1,a,x)),Times(C1D2,n)),CN1)),x)),And(And(FreeQ(List(a,c,d,n,p),x),ZeroQ(Plus(Times(Sqr(a),Sqr(c)),Times(CN1,Sqr(d))))),Or(IntegerQ(p),PositiveQ(c))))),
ISetDelayed(Int(Times(u_DEFAULT,Power(Plus(c_,Times($p(d,true),x_)),p_DEFAULT),Power(E,Times(n_DEFAULT,ArcTanh(Times(a_DEFAULT,x_))))),x_Symbol),
    Condition(Int(Times(u,Power(Plus(c,Times(d,x)),p),Power(Plus(C1,Times(a,x)),Times(C1D2,n)),Power(Power(Plus(C1,Times(CN1,a,x)),Times(C1D2,n)),CN1)),x),And(And(FreeQ(List(a,c,d,n,p),x),ZeroQ(Plus(Times(Sqr(a),Sqr(c)),Times(CN1,Sqr(d))))),Not(Or(IntegerQ(p),PositiveQ(c)))))),
ISetDelayed(Int(Times(u_DEFAULT,Power(Plus(c_,Times($p(d,true),Power(x_,CN1))),p_DEFAULT),Power(E,Times(n_DEFAULT,ArcTanh(Times(a_DEFAULT,x_))))),x_Symbol),
    Condition(Times(Power(d,p),Int(Times(u,Power(Plus(C1,Times(c,x,Power(d,CN1))),p),Power(E,Times(n,ArcTanh(Times(a,x)))),Power(Power(x,p),CN1)),x)),And(And(FreeQ(List(a,c,d,n),x),ZeroQ(Plus(Sqr(c),Times(CN1,Sqr(a),Sqr(d))))),IntegerQ(p)))),
ISetDelayed(Int(Times(u_DEFAULT,Power(Plus(c_,Times($p(d,true),Power(x_,CN1))),p_),Power(E,Times(n_,ArcTanh(Times(a_DEFAULT,x_))))),x_Symbol),
    Condition(Times(Power(CN1,Times(C1D2,n)),Power(c,p),Int(Times(u,Power(Plus(C1,Times(d,Power(Times(c,x),CN1))),p),Power(Plus(C1,Power(Times(a,x),CN1)),Times(C1D2,n)),Power(Power(Plus(C1,Times(CN1,Power(Times(a,x),CN1))),Times(C1D2,n)),CN1)),x)),And(And(And(And(FreeQ(List(a,c,d,p),x),ZeroQ(Plus(Sqr(c),Times(CN1,Sqr(a),Sqr(d))))),Not(IntegerQ(p))),IntegerQ(Times(C1D2,n))),PositiveQ(c)))),
ISetDelayed(Int(Times(u_DEFAULT,Power(Plus(c_,Times($p(d,true),Power(x_,CN1))),p_),Power(E,Times(n_,ArcTanh(Times(a_DEFAULT,x_))))),x_Symbol),
    Condition(Int(Times(u,Power(Plus(c,Times(d,Power(x,CN1))),p),Power(Plus(C1,Times(a,x)),Times(C1D2,n)),Power(Power(Plus(C1,Times(CN1,a,x)),Times(C1D2,n)),CN1)),x),And(And(And(And(FreeQ(List(a,c,d,p),x),ZeroQ(Plus(Sqr(c),Times(CN1,Sqr(a),Sqr(d))))),Not(IntegerQ(p))),IntegerQ(Times(C1D2,n))),Not(PositiveQ(c))))),
ISetDelayed(Int(Times(u_DEFAULT,Power(Plus(c_,Times($p(d,true),Power(x_,CN1))),p_),Power(E,Times(n_DEFAULT,ArcTanh(Times(a_DEFAULT,x_))))),x_Symbol),
    Condition(Times(Power(x,p),Power(Plus(c,Times(d,Power(x,CN1))),p),Power(Power(Plus(C1,Times(c,x,Power(d,CN1))),p),CN1),Int(Times(u,Power(Plus(C1,Times(c,x,Power(d,CN1))),p),Power(E,Times(n,ArcTanh(Times(a,x)))),Power(Power(x,p),CN1)),x)),And(And(FreeQ(List(a,c,d,n,p),x),ZeroQ(Plus(Sqr(c),Times(CN1,Sqr(a),Sqr(d))))),Not(IntegerQ(p))))),
ISetDelayed(Int(Times(Power(E,Times(n_DEFAULT,ArcTanh(Times(a_DEFAULT,x_)))),Power(Plus(c_,Times($p(d,true),Sqr(x_))),CN1)),x_Symbol),
    Condition(Times(Power(E,Times(n,ArcTanh(Times(a,x)))),Power(Times(a,c,n),CN1)),And(And(FreeQ(List(a,c,d,n),x),ZeroQ(Plus(Times(Sqr(a),c),d))),Not(IntegerQ(Times(C1D2,n)))))),
ISetDelayed(Int(Times(Power(E,Times(n_DEFAULT,ArcTanh(Times(a_DEFAULT,x_)))),Power(Plus(C1,Times($p(d,true),Sqr(x_))),CN1D2)),x_Symbol),
    Condition(Int(Power(Plus(C1,Times(CN1,a,n,x)),CN1),x),And(And(FreeQ(List(a,d,n),x),ZeroQ(Plus(d,Sqr(a)))),ZeroQ(Plus(Sqr(n),Times(CN1,C1)))))),
ISetDelayed(Int(Times(Power(E,Times(n_DEFAULT,ArcTanh(Times(a_DEFAULT,x_)))),Power(Plus(c_DEFAULT,Times($p(d,true),Sqr(x_))),CN1D2)),x_Symbol),
    Condition(Times(Sqrt(Plus(C1,Times(CN1,Sqr(a),Sqr(x)))),Power(Plus(c,Times(d,Sqr(x))),CN1D2),Int(Times(Power(E,Times(n,ArcTanh(Times(a,x)))),Power(Plus(C1,Times(CN1,Sqr(a),Sqr(x))),CN1D2)),x)),And(And(And(FreeQ(List(a,c,d,n),x),ZeroQ(Plus(Times(Sqr(a),c),d))),ZeroQ(Plus(Sqr(n),Times(CN1,C1)))),NonzeroQ(Plus(c,Times(CN1,C1)))))),
ISetDelayed(Int(Times(Power(E,Times(n_DEFAULT,ArcTanh(Times(a_DEFAULT,x_)))),Power(Plus(c_,Times($p(d,true),Sqr(x_))),p_DEFAULT)),x_Symbol),
    Condition(Times(Plus(n,Times(C2,a,p,x)),Power(Plus(c,Times(d,Sqr(x))),p),Power(E,Times(n,ArcTanh(Times(a,x)))),Power(Times(C2,a,p,Plus(Times(C2,p),C1)),CN1)),And(And(And(And(FreeQ(List(a,c,d,n,p),x),ZeroQ(Plus(Times(Sqr(a),c),d))),ZeroQ(Plus(Sqr(n),Times(CN1,C4,Sqr(p))))),NonzeroQ(Plus(p,C1D2))),Not(IntegerQ(n))))),
ISetDelayed(Int(Times(Power(E,Times(n_DEFAULT,ArcTanh(Times(a_DEFAULT,x_)))),Power(Plus(c_,Times($p(d,true),Sqr(x_))),QQ(-3L,2L))),x_Symbol),
    Condition(Times(Power(c,QQ(-3L,2L)),Int(Power(Times(Plus(C1,Times(a,n,x)),Sqr(Plus(C1,Times(CN1,a,n,x)))),CN1),x)),And(And(And(FreeQ(List(a,c,d),x),ZeroQ(Plus(Times(Sqr(a),c),d))),ZeroQ(Plus(Sqr(n),Times(CN1,C1)))),PositiveQ(c)))),
ISetDelayed(Int(Times(Power(E,Times(n_DEFAULT,ArcTanh(Times(a_DEFAULT,x_)))),Power(Plus(c_,Times($p(d,true),Sqr(x_))),QQ(-3L,2L))),x_Symbol),
    Condition(Times(Sqrt(Plus(C1,Times(CN1,Sqr(a),Sqr(x)))),Power(Times(c,Sqrt(Plus(c,Times(d,Sqr(x))))),CN1),Int(Times(Power(E,Times(n,ArcTanh(Times(a,x)))),Power(Plus(C1,Times(CN1,Sqr(a),Sqr(x))),QQ(-3L,2L))),x)),And(And(And(FreeQ(List(a,c,d),x),ZeroQ(Plus(Times(Sqr(a),c),d))),ZeroQ(Plus(Sqr(n),Times(CN1,C1)))),Not(PositiveQ(c))))),
ISetDelayed(Int(Times(Power(E,Times(n_,ArcTanh(Times(a_DEFAULT,x_)))),Power(Plus(c_,Times($p(d,true),Sqr(x_))),QQ(-3L,2L))),x_Symbol),
    Condition(Times(Plus(n,Times(CN1,a,x)),Power(E,Times(n,ArcTanh(Times(a,x)))),Power(Times(a,c,Plus(Sqr(n),Times(CN1,C1)),Sqrt(Plus(c,Times(d,Sqr(x))))),CN1)),And(And(And(FreeQ(List(a,c,d,n),x),ZeroQ(Plus(Times(Sqr(a),c),d))),NonzeroQ(Plus(Sqr(n),Times(CN1,C9)))),NonzeroQ(Plus(Sqr(n),Times(CN1,C1)))))),
ISetDelayed(Int(Times(Power(E,Times(n_,ArcTanh(Times(a_DEFAULT,x_)))),Power(Plus(c_,Times($p(d,true),Sqr(x_))),p_)),x_Symbol),
    Condition(Plus(Times(Plus(n,Times(C2,a,Plus(p,C1),x)),Power(Plus(c,Times(d,Sqr(x))),Plus(p,C1)),Power(E,Times(n,ArcTanh(Times(a,x)))),Power(Times(a,c,Plus(Sqr(n),Times(CN1,C4,Sqr(Plus(p,C1))))),CN1)),Times(CN1,C2,Plus(p,C1),Plus(Times(C2,p),C3),Power(Times(c,Plus(Sqr(n),Times(CN1,C4,Sqr(Plus(p,C1))))),CN1),Int(Times(Power(Plus(c,Times(d,Sqr(x))),Plus(p,C1)),Power(E,Times(n,ArcTanh(Times(a,x))))),x))),And(And(And(And(And(And(And(FreeQ(List(a,c,d,n),x),ZeroQ(Plus(Times(Sqr(a),c),d))),NonzeroQ(Plus(Sqr(n),Times(CN1,C4,Sqr(p))))),RationalQ(p)),Less(p,CN1)),Unequal(p,QQ(-3L,2L))),NonzeroQ(Plus(Sqr(n),Times(CN1,C4,Sqr(Plus(p,C1)))))),Not(IntegerQ(n))))),
ISetDelayed(Int(Times(Power(E,Times(n_DEFAULT,ArcTanh(Times(a_DEFAULT,x_)))),Power(Plus(c_,Times($p(d,true),Sqr(x_))),p_DEFAULT)),x_Symbol),
    Condition(Times(Power(c,p),Int(Times(Power(Plus(C1,Times(CN1,Sqr(a),Sqr(x))),Plus(p,Times(CN1,C1D2,n))),Power(Plus(C1,Times(a,x)),n)),x)),And(And(And(FreeQ(List(a,c,d,p),x),ZeroQ(Plus(Times(Sqr(a),c),d))),Or(IntegerQ(p),PositiveQ(c))),OddQ(n)))),
ISetDelayed(Int(Times(Power(E,Times(n_DEFAULT,ArcTanh(Times(a_DEFAULT,x_)))),Power(Plus(c_,Times($p(d,true),Sqr(x_))),p_DEFAULT)),x_Symbol),
    Condition(Times(Power(c,p),Int(Times(Power(Plus(C1,Times(CN1,a,x)),Plus(p,Times(CN1,C1D2,n))),Power(Plus(C1,Times(a,x)),Plus(p,Times(C1D2,n)))),x)),And(And(And(And(FreeQ(List(a,c,d,n,p),x),ZeroQ(Plus(Times(Sqr(a),c),d))),Or(IntegerQ(p),PositiveQ(c))),Not(OddQ(n))),Or(Or(RationalQ(n,p),PositiveIntegerQ(Plus(p,Times(CN1,C1D2,n)))),PositiveIntegerQ(Plus(p,Times(C1D2,n))))))),
ISetDelayed(Int(Times(Power(E,Times(n_,ArcTanh(Times(a_DEFAULT,x_)))),Power(Plus(c_,Times($p(d,true),Sqr(x_))),p_)),x_Symbol),
    Condition(Times(Power(c,Times(C1D2,n)),Int(Times(Power(Plus(c,Times(d,Sqr(x))),Plus(p,Times(CN1,C1D2,n))),Power(Plus(C1,Times(a,x)),n)),x)),And(And(And(And(FreeQ(List(a,c,d,n,p),x),ZeroQ(Plus(Times(Sqr(a),c),d))),Not(Or(IntegerQ(p),PositiveQ(c)))),EvenQ(n)),RationalQ(p)))),
ISetDelayed(Int(Times(Power(E,Times(n_DEFAULT,ArcTanh(Times(a_DEFAULT,x_)))),Power(Plus(c_,Times($p(d,true),Sqr(x_))),p_)),x_Symbol),
    Condition(Times(Power(c,Plus(p,Times(CN1,C1D2))),Sqrt(Plus(c,Times(d,Sqr(x)))),Power(Plus(C1,Times(CN1,Sqr(a),Sqr(x))),CN1D2),Int(Times(Power(Plus(C1,Times(CN1,Sqr(a),Sqr(x))),p),Power(E,Times(n,ArcTanh(Times(a,x))))),x)),And(And(And(And(FreeQ(List(a,c,d,n,p),x),ZeroQ(Plus(Times(Sqr(a),c),d))),Not(PositiveQ(c))),PositiveIntegerQ(Plus(p,C1D2))),RationalQ(n)))),
ISetDelayed(Int(Times(Power(E,Times(n_DEFAULT,ArcTanh(Times(a_DEFAULT,x_)))),Power(Plus(c_,Times($p(d,true),Sqr(x_))),p_)),x_Symbol),
    Condition(Times(Power(c,Plus(p,C1D2)),Sqrt(Plus(C1,Times(CN1,Sqr(a),Sqr(x)))),Power(Plus(c,Times(d,Sqr(x))),CN1D2),Int(Times(Power(Plus(C1,Times(CN1,Sqr(a),Sqr(x))),p),Power(E,Times(n,ArcTanh(Times(a,x))))),x)),And(And(And(And(FreeQ(List(a,c,d,n,p),x),ZeroQ(Plus(Times(Sqr(a),c),d))),Not(PositiveQ(c))),NegativeIntegerQ(Plus(p,Times(CN1,C1D2)))),RationalQ(n)))),
ISetDelayed(Int(Times(Power(E,Times(n_DEFAULT,ArcTanh(Times(a_DEFAULT,x_)))),Power(Plus(c_,Times($p(d,true),Sqr(x_))),p_)),x_Symbol),
    Condition(Times(Power(Plus(c,Times(d,Sqr(x))),p),Power(Power(Plus(C1,Times(CN1,Sqr(a),Sqr(x))),p),CN1),Int(Times(Power(Plus(C1,Times(CN1,Sqr(a),Sqr(x))),p),Power(E,Times(n,ArcTanh(Times(a,x))))),x)),And(And(And(And(FreeQ(List(a,c,d,n,p),x),ZeroQ(Plus(Times(Sqr(a),c),d))),Not(Or(IntegerQ(p),PositiveQ(c)))),Not(IntegerQ(Plus(p,C1D2)))),Or(Or(RationalQ(n,p),PositiveIntegerQ(Plus(p,Times(CN1,C1D2,n)))),PositiveIntegerQ(Plus(p,Times(C1D2,n))))))),
ISetDelayed(Int(Times(Power(E,Times(n_DEFAULT,ArcTanh(Times(a_DEFAULT,x_)))),Power(Plus(c_,Times($p(d,true),Sqr(x_))),p_DEFAULT)),x_Symbol),
    Condition(Times(Power(Plus(c,Times(d,Sqr(x))),Plus(p,C1)),Power(E,Times(n,ArcTanh(Times(a,x)))),Power(Times(C2,a,c,n),CN1),Power(Plus(C1,Power(E,Times(n,Power(Plus(p,C1),CN1),ArcTanh(Times(a,x))))),Times(C2,Plus(p,C1))),Hypergeometric2F1(Times(C2,Plus(p,C1)),Times(C2,Plus(p,C1)),Plus(Times(C2,p),C3),Times(CN1,Power(E,Times(n,Power(Plus(p,C1),CN1),ArcTanh(Times(a,x))))))),And(And(And(FreeQ(List(a,c,d,n,p),x),ZeroQ(Plus(Times(Sqr(a),c),d))),ZeroQ(Plus(Sqr(n),Times(CN1,C4,Sqr(Plus(p,C1)))))),Not(IntegerQ(Times(C2,p)))))),
ISetDelayed(Int(Times(Power(E,Times(n_DEFAULT,ArcTanh(Times(a_DEFAULT,x_)))),Power(Plus(c_,Times($p(d,true),Sqr(x_))),p_DEFAULT)),x_Symbol),
    Condition(Times(Power(C4,Plus(p,C1)),Power(Plus(c,Times(d,Sqr(x))),Plus(p,C1)),Power(E,Times(n,ArcTanh(Times(a,x)))),Power(Times(a,c,Plus(n,Times(CN1,C2,Plus(p,C1)))),CN1),Power(Power(Plus(C1,Times(a,x)),CN1),Times(C2,Plus(p,C1))),Hypergeometric2F1(Plus(p,Times(CN1,C1D2,n),C1),Times(C2,Plus(p,C1)),Plus(p,Times(CN1,C1D2,n),C2),Times(CN1,Power(E,Times(CN2,ArcTanh(Times(a,x))))))),And(And(And(And(FreeQ(List(a,c,d,n,p),x),ZeroQ(Plus(Times(Sqr(a),c),d))),NonzeroQ(Plus(Sqr(n),Times(CN1,C4,Sqr(Plus(p,C1)))))),Not(NegativeIntegerQ(Plus(Times(C2,p),C1)))),Not(IntegerQ(Plus(p,Times(CN1,C1D2,n))))))),
ISetDelayed(Int(Times(x_,Power(E,Times(n_,ArcTanh(Times(a_DEFAULT,x_)))),Power(Plus(c_,Times($p(d,true),Sqr(x_))),QQ(-3L,2L))),x_Symbol),
    Condition(Times(CN1,Plus(C1,Times(CN1,a,n,x)),Power(E,Times(n,ArcTanh(Times(a,x)))),Power(Times(Sqr(a),c,Plus(Sqr(n),Times(CN1,C1)),Sqrt(Plus(c,Times(d,Sqr(x))))),CN1)),And(And(FreeQ(List(a,c,d,n),x),ZeroQ(Plus(Times(Sqr(a),c),d))),Not(IntegerQ(n))))),
ISetDelayed(Int(Times(x_,Power(Plus(c_,Times($p(d,true),Sqr(x_))),p_),Power(E,Times(n_DEFAULT,ArcTanh(Times(a_DEFAULT,x_))))),x_Symbol),
    Condition(Plus(Times(Plus(Times(C2,Plus(p,C1)),Times(a,n,x)),Power(Plus(c,Times(d,Sqr(x))),Plus(p,C1)),Power(E,Times(n,ArcTanh(Times(a,x)))),Power(Times(Sqr(a),c,Plus(Sqr(n),Times(CN1,C4,Sqr(Plus(p,C1))))),CN1)),Times(CN1,n,Plus(Times(C2,p),C3),Power(Times(a,c,Plus(Sqr(n),Times(CN1,C4,Sqr(Plus(p,C1))))),CN1),Int(Times(Power(Plus(c,Times(d,Sqr(x))),Plus(p,C1)),Power(E,Times(n,ArcTanh(Times(a,x))))),x))),And(And(And(And(And(And(FreeQ(List(a,c,d,n),x),ZeroQ(Plus(Times(Sqr(a),c),d))),RationalQ(p)),LessEqual(p,CN1)),Unequal(p,QQ(-3L,2L))),NonzeroQ(Plus(Sqr(n),Times(CN1,C4,Sqr(Plus(p,C1)))))),Not(IntegerQ(n))))),
ISetDelayed(Int(Times(Sqr(x_),Power(Plus(c_,Times($p(d,true),Sqr(x_))),p_DEFAULT),Power(E,Times(n_DEFAULT,ArcTanh(Times(a_DEFAULT,x_))))),x_Symbol),
    Condition(Times(CN1,Plus(n,Times(C2,Plus(p,C1),a,x)),Power(Plus(c,Times(d,Sqr(x))),Plus(p,C1)),Power(E,Times(n,ArcTanh(Times(a,x)))),Power(Times(Power(a,C3),c,Sqr(n),Plus(Sqr(n),Times(CN1,C1))),CN1)),And(And(And(FreeQ(List(a,c,d,n),x),ZeroQ(Plus(Times(Sqr(a),c),d))),ZeroQ(Plus(Sqr(n),Times(C2,Plus(p,C1))))),Not(IntegerQ(n))))),
ISetDelayed(Int(Times(Sqr(x_),Power(Plus(c_,Times($p(d,true),Sqr(x_))),p_),Power(E,Times(n_DEFAULT,ArcTanh(Times(a_DEFAULT,x_))))),x_Symbol),
    Condition(Plus(Times(Plus(n,Times(C2,Plus(p,C1),a,x)),Power(Plus(c,Times(d,Sqr(x))),Plus(p,C1)),Power(E,Times(n,ArcTanh(Times(a,x)))),Power(Times(Power(a,C3),c,Plus(Sqr(n),Times(CN1,C4,Sqr(Plus(p,C1))))),CN1)),Times(CN1,Plus(Sqr(n),Times(C2,Plus(p,C1))),Power(Times(Sqr(a),c,Plus(Sqr(n),Times(CN1,C4,Sqr(Plus(p,C1))))),CN1),Int(Times(Power(Plus(c,Times(d,Sqr(x))),Plus(p,C1)),Power(E,Times(n,ArcTanh(Times(a,x))))),x))),And(And(And(And(And(And(FreeQ(List(a,c,d,n),x),ZeroQ(Plus(Times(Sqr(a),c),d))),RationalQ(p)),LessEqual(p,CN1)),NonzeroQ(Plus(Sqr(n),Times(C2,Plus(p,C1))))),NonzeroQ(Plus(Sqr(n),Times(CN1,C4,Sqr(Plus(p,C1)))))),Not(IntegerQ(n))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Plus(c_,Times($p(d,true),Sqr(x_))),p_),Power(E,Times(n_DEFAULT,ArcTanh(Times(a_DEFAULT,x_))))),x_Symbol),
    Condition(Times(Power(c,p),Power(Power(a,Plus(m,C1)),CN1),Subst(Int(Times(Power(E,Times(n,x)),Power(Tanh(x),Plus(m,Times(C2,Plus(p,C1)))),Power(Power(Sinh(x),Times(C2,Plus(p,C1))),CN1)),x),x,ArcTanh(Times(a,x)))),And(And(And(And(And(And(FreeQ(List(a,c,d,n),x),ZeroQ(Plus(Times(Sqr(a),c),d))),IntegerQ(m)),RationalQ(p)),LessEqual(LessEqual(C3,m),Times(CN2,Plus(p,C1)))),IntegerQ(p)),Not(IntegerQ(n))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Plus(c_,Times($p(d,true),Sqr(x_))),p_DEFAULT),Power(E,Times(n_DEFAULT,ArcTanh(Times(a_DEFAULT,x_))))),x_Symbol),
    Condition(Times(Power(c,p),Int(Times(Power(x,m),Power(Plus(C1,Times(CN1,a,x)),Plus(p,Times(CN1,C1D2,n))),Power(Plus(C1,Times(a,x)),Plus(p,Times(C1D2,n)))),x)),And(And(And(And(And(FreeQ(List(a,c,d,m,n,p),x),ZeroQ(Plus(Times(Sqr(a),c),d))),Or(IntegerQ(p),PositiveQ(c))),ZeroQ(Plus(Sqr(n),Times(CN1,C1)))),Or(Not(RationalQ(m)),Not(RationalQ(p)))),Not(IntegerQ(Plus(p,Times(CN1,C1D2,n))))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Plus(c_,Times($p(d,true),Sqr(x_))),p_DEFAULT),Power(E,Times(n_DEFAULT,ArcTanh(Times(a_DEFAULT,x_))))),x_Symbol),
    Condition(Times(Power(c,p),Int(Times(Power(x,m),Power(Plus(C1,Times(CN1,Sqr(a),Sqr(x))),Plus(p,Times(CN1,C1D2,n))),Power(Plus(C1,Times(a,x)),n)),x)),And(And(And(And(And(FreeQ(List(a,c,d,m,n,p),x),ZeroQ(Plus(Times(Sqr(a),c),d))),Or(IntegerQ(p),PositiveQ(c))),OddQ(n)),Or(Or(Not(RationalQ(m)),Not(RationalQ(p))),And(ZeroQ(Plus(n,Times(CN1,C1))),NonzeroQ(Plus(p,C1))))),Not(IntegerQ(Plus(p,Times(CN1,C1D2,n))))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Plus(c_,Times($p(d,true),Sqr(x_))),p_),Power(E,Times(n_,ArcTanh(Times(a_DEFAULT,x_))))),x_Symbol),
    Condition(Times(Power(c,Times(C1D2,n)),Int(Times(Power(x,m),Power(Plus(c,Times(d,Sqr(x))),Plus(p,Times(CN1,C1D2,n))),Power(Plus(C1,Times(a,x)),n)),x)),And(And(And(And(FreeQ(List(a,c,d,m,n,p),x),ZeroQ(Plus(Times(Sqr(a),c),d))),Not(Or(IntegerQ(p),PositiveQ(c)))),IntegerQ(Times(C1D2,n))),Or(ZeroQ(Plus(m,Times(CN1,C1))),Not(IntegerQ(Plus(p,C1D2))))))),
ISetDelayed(Int(Times(u_,Power(Plus(c_,Times($p(d,true),Sqr(x_))),p_DEFAULT),Power(E,Times(n_DEFAULT,ArcTanh(Times(a_DEFAULT,x_))))),x_Symbol),
    Condition(Times(Power(c,p),Int(Times(u,Power(Plus(C1,Times(CN1,a,x)),Plus(p,Times(CN1,C1D2,n))),Power(Plus(C1,Times(a,x)),Plus(p,Times(C1D2,n)))),x)),And(And(FreeQ(List(a,c,d,n,p),x),ZeroQ(Plus(Times(Sqr(a),c),d))),Or(IntegerQ(p),PositiveQ(c))))),
ISetDelayed(Int(Times(u_,Power(Plus(c_,Times($p(d,true),Sqr(x_))),p_),Power(E,Times(n_,ArcTanh(Times(a_DEFAULT,x_))))),x_Symbol),
    Condition(Times(Power(c,Plus(p,Times(CN1,C1D2))),Sqrt(Plus(c,Times(d,Sqr(x)))),Power(Times(Sqrt(Plus(C1,Times(CN1,a,x))),Sqrt(Plus(C1,Times(a,x)))),CN1),Int(Times(u,Power(Plus(C1,Times(CN1,a,x)),Plus(p,Times(CN1,C1D2,n))),Power(Plus(C1,Times(a,x)),Plus(p,Times(C1D2,n)))),x)),And(And(And(And(FreeQ(List(a,c,d,n,p),x),ZeroQ(Plus(Times(Sqr(a),c),d))),Not(PositiveQ(c))),IntegerQ(Times(C1D2,n))),PositiveIntegerQ(Plus(p,C1D2))))),
ISetDelayed(Int(Times(u_,Power(Plus(c_,Times($p(d,true),Sqr(x_))),p_),Power(E,Times(n_,ArcTanh(Times(a_DEFAULT,x_))))),x_Symbol),
    Condition(Times(Power(c,Plus(p,C1D2)),Sqrt(Plus(C1,Times(CN1,a,x))),Sqrt(Plus(C1,Times(a,x))),Power(Plus(c,Times(d,Sqr(x))),CN1D2),Int(Times(u,Power(Plus(C1,Times(CN1,a,x)),Plus(p,Times(CN1,C1D2,n))),Power(Plus(C1,Times(a,x)),Plus(p,Times(C1D2,n)))),x)),And(And(And(And(FreeQ(List(a,c,d,n,p),x),ZeroQ(Plus(Times(Sqr(a),c),d))),Not(PositiveQ(c))),IntegerQ(Times(C1D2,n))),NegativeIntegerQ(Plus(p,Times(CN1,C1D2)))))),
ISetDelayed(Int(Times(u_,Power(Plus(c_,Times($p(d,true),Sqr(x_))),p_),Power(E,Times(n_DEFAULT,ArcTanh(Times(a_DEFAULT,x_))))),x_Symbol),
    Condition(Times(Power(c,Plus(p,Times(CN1,C1D2))),Sqrt(Plus(c,Times(d,Sqr(x)))),Power(Plus(C1,Times(CN1,Sqr(a),Sqr(x))),CN1D2),Int(Times(u,Power(Plus(C1,Times(CN1,Sqr(a),Sqr(x))),p),Power(E,Times(n,ArcTanh(Times(a,x))))),x)),And(And(And(And(FreeQ(List(a,c,d,n,p),x),ZeroQ(Plus(Times(Sqr(a),c),d))),Not(PositiveQ(c))),Not(IntegerQ(Times(C1D2,n)))),PositiveIntegerQ(Plus(p,C1D2)))))
  );
}
