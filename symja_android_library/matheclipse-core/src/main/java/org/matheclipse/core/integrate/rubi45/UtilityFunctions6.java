package org.matheclipse.core.integrate.rubi45;


import static org.matheclipse.core.expression.F.*;
import static org.matheclipse.core.integrate.rubi45.UtilityFunctionCtors.*;
import static org.matheclipse.core.integrate.rubi45.UtilityFunctions.*;

import org.matheclipse.core.interfaces.IAST;
import org.matheclipse.core.interfaces.IExpr;
import org.matheclipse.core.interfaces.ISymbol;
/** 
 * UtilityFunctions rules from the <a href="http://www.apmaths.uwo.ca/~arich/">Rubi -
 * rule-based integrator</a>.
 *  
 */
public class UtilityFunctions6 { 
  public static IAST RULES = List( 
ISetDelayed(FixInertTrigFunction(Times(u_DEFAULT,Power($($s("§csc"),v_),m_DEFAULT),Power(Plus(Times(b_DEFAULT,$($s("§cos"),v_)),Times(a_DEFAULT,$($s("§sin"),v_))),n_DEFAULT)),x_),
    Condition(FixInertTrigFunction(Times(u,Power($($s("§sin"),v),Times(CN1,m)),Power(Plus(Times(a,$($s("§sin"),v)),Times(b,$($s("§cos"),v))),n)),x),And(FreeQ(List(a,b,n),x),IntegerQ(m)))),
ISetDelayed(FixInertTrigFunction(Times(Plus($p("A",true),Times($p("B",true),$(g_,v_)),Times($p("C",true),Sqr($(g_,v_)))),Power($(f_,v_),m_DEFAULT)),x_),
    Condition(Times(Power($(g,v),Times(CN1,m)),Plus($s("A"),Times($s("B"),$(g,v)),Times($s("C"),Sqr($(g,v))))),And(And(FreeQ(List($s("A"),$s("B"),$s("C")),x),IntegerQ(m)),Or(InertReciprocalQ(f,g),InertReciprocalQ(g,f))))),
ISetDelayed(FixInertTrigFunction(Times(Plus($p("A",true),Times($p("C",true),Sqr($(g_,v_)))),Power($(f_,v_),m_DEFAULT)),x_),
    Condition(Times(Power($(g,v),Times(CN1,m)),Plus($s("A"),Times($s("C"),Sqr($(g,v))))),And(And(FreeQ(List($s("A"),$s("C")),x),IntegerQ(m)),Or(InertReciprocalQ(f,g),InertReciprocalQ(g,f))))),
ISetDelayed(FixInertTrigFunction(Times(Plus($p("A",true),Times($p("B",true),$(g_,v_)),Times($p("C",true),Sqr($(g_,v_)))),Power(Plus(a_DEFAULT,Times(b_DEFAULT,$(g_,v_))),n_DEFAULT),Power($(f_,v_),m_DEFAULT)),x_),
    Condition(Times(Power($(g,v),Times(CN1,m)),Plus($s("A"),Times($s("B"),$(g,v)),Times($s("C"),Sqr($(g,v)))),Power(Plus(a,Times(b,$(g,v))),n)),And(And(FreeQ(List(a,b,$s("A"),$s("B"),$s("C"),n),x),IntegerQ(m)),Or(InertReciprocalQ(f,g),InertReciprocalQ(g,f))))),
ISetDelayed(FixInertTrigFunction(Times(Plus($p("A",true),Times($p("C",true),Sqr($(g_,v_)))),Power($(f_,v_),m_DEFAULT),Power(Plus(a_DEFAULT,Times(b_DEFAULT,$(g_,v_))),n_DEFAULT)),x_),
    Condition(Times(Power($(g,v),Times(CN1,m)),Plus($s("A"),Times($s("C"),Sqr($(g,v)))),Power(Plus(a,Times(b,$(g,v))),n)),And(And(FreeQ(List(a,b,$s("A"),$s("C"),n),x),IntegerQ(m)),Or(InertReciprocalQ(f,g),InertReciprocalQ(g,f))))),
ISetDelayed(FixInertTrigFunction(u_,x_),
    u),
ISetDelayed(InertTrigSumQ(u_,$p("func"),x_),
    Or(MatchQ(u,Condition(Power(Plus(a_,Times(b_DEFAULT,Power(Times(c_DEFAULT,$($s("func"),w_)),p_DEFAULT))),n_DEFAULT),FreeQ(List(a,b,c,n,p),x))),MatchQ(u,Condition(Power(Plus(a_DEFAULT,Times(b_DEFAULT,Power(Times($p(d,true),$($s("func"),w_)),p_DEFAULT)),Times(c_DEFAULT,Power(Times($p(d,true),$($s("func"),w_)),q_DEFAULT))),n_DEFAULT),FreeQ(List(a,b,c,d,n,p,q),x))))),
ISetDelayed(KnownSineIntegrandQ(u_,x_Symbol),
    KnownTrigIntegrandQ(List($s("§sin"),$s("§cos")),u,x)),
ISetDelayed(KnownTangentIntegrandQ(u_,x_Symbol),
    KnownTrigIntegrandQ(List($s("§tan")),u,x)),
ISetDelayed(KnownCotangentIntegrandQ(u_,x_Symbol),
    KnownTrigIntegrandQ(List($s("§cot")),u,x)),
ISetDelayed(KnownSecantIntegrandQ(u_,x_Symbol),
    KnownTrigIntegrandQ(List($s("§sec"),$s("§csc")),u,x)),
ISetDelayed(KnownTrigIntegrandQ($p("§list"),u_,x_Symbol),
    Or(Or(Or(Or(Or(Or(SameQ(u,C1),MatchQ(u,Condition(Power(Plus(a_DEFAULT,Times(b_DEFAULT,$($p("func"),Plus($p(e,true),Times(f_DEFAULT,x))))),m_DEFAULT),And(MemberQ($s("§list"),$s("func")),FreeQ(List(a,b,e,f,m),x))))),MatchQ(u,Condition(Times(Power(Plus(a_DEFAULT,Times(b_DEFAULT,$($p("func"),Plus($p(e,true),Times(f_DEFAULT,x))))),m_DEFAULT),Plus($p("A",true),Times($p("B",true),$($p("func"),Plus($p(e,true),Times(f_DEFAULT,x)))))),And(MemberQ($s("§list"),$s("func")),FreeQ(List(a,b,e,f,$s("A"),$s("B"),m),x))))),MatchQ(u,Condition(Plus($p("A",true),Times($p("C",true),Sqr($($p("func"),Plus($p(e,true),Times(f_DEFAULT,x)))))),And(MemberQ($s("§list"),$s("func")),FreeQ(List(e,f,$s("A"),$s("C")),x))))),MatchQ(u,Condition(Plus($p("A",true),Times($p("B",true),$($p("func"),Plus($p(e,true),Times(f_DEFAULT,x)))),Times($p("C",true),Sqr($($p("func"),Plus($p(e,true),Times(f_DEFAULT,x)))))),And(MemberQ($s("§list"),$s("func")),FreeQ(List(e,f,$s("A"),$s("B"),$s("C")),x))))),MatchQ(u,Condition(Times(Power(Plus(a_DEFAULT,Times(b_DEFAULT,$($p("func"),Plus($p(e,true),Times(f_DEFAULT,x))))),m_DEFAULT),Plus($p("A",true),Times($p("C",true),Sqr($($p("func"),Plus($p(e,true),Times(f_DEFAULT,x))))))),And(MemberQ($s("§list"),$s("func")),FreeQ(List(a,b,e,f,$s("A"),$s("C"),m),x))))),MatchQ(u,Condition(Times(Power(Plus(a_DEFAULT,Times(b_DEFAULT,$($p("func"),Plus($p(e,true),Times(f_DEFAULT,x))))),m_DEFAULT),Plus($p("A",true),Times($p("B",true),$($p("func"),Plus($p(e,true),Times(f_DEFAULT,x)))),Times($p("C",true),Sqr($($p("func"),Plus($p(e,true),Times(f_DEFAULT,x))))))),And(MemberQ($s("§list"),$s("func")),FreeQ(List(a,b,e,f,$s("A"),$s("B"),$s("C"),m),x)))))),
ISetDelayed(PiecewiseLinearQ(u_,v_,x_Symbol),
    And(PiecewiseLinearQ(u,x),PiecewiseLinearQ(v,x))),
ISetDelayed(PiecewiseLinearQ(u_,x_Symbol),
    Or(Or(LinearQ(u,x),MatchQ(u,Condition(Log(Times(c_DEFAULT,Power($p("F"),v_))),And(FreeQ(List($s("F"),c),x),LinearQ(v,x))))),MatchQ(u,Condition($($p("F"),$($p("G"),v_)),And(LinearQ(v,x),MemberQ(List(List($s("ArcTanh"),$s("Tanh")),List($s("ArcTanh"),$s("Coth")),List($s("ArcCoth"),$s("Coth")),List($s("ArcCoth"),$s("Tanh")),List($s("ArcTan"),$s("Tan")),List($s("ArcTan"),$s("Cot")),List($s("ArcCot"),$s("Cot")),List($s("ArcCot"),$s("Tan"))),List($s("F"),$s("G")))))))),
ISetDelayed(Divides(y_,u_,x_Symbol),
    Module(List(Set(v,Simplify(Times(u,Power(y,CN1))))),If(FreeQ(v,x),v,False))),
ISetDelayed(DerivativeDivides(y_,u_,x_Symbol),
    If(MatchQ(y,Condition(Times(a_DEFAULT,x),FreeQ(a,x))),False,If(If(PolynomialQ(y,x),And(PolynomialQ(u,x),Equal(Exponent(u,x),Plus(Exponent(y,x),Times(CN1,C1)))),EasyDQ(y,x)),Module(List(Set(v,Block(List(Set($s("§showsteps"),False)),D(y,x)))),If(ZeroQ(v),False,CompoundExpression(Set(v,Simplify(Times(u,Power(v,CN1)))),If(FreeQ(v,x),v,False)))),False))),
ISetDelayed(EasyDQ(Times(u_DEFAULT,Power(x_,m_DEFAULT)),x_Symbol),
    Condition(EasyDQ(u,x),FreeQ(m,x))),
ISetDelayed(EasyDQ(u_,x_Symbol),
    If(Or(Or(AtomQ(u),FreeQ(u,x)),Equal(Length(u),C0)),True,If(CalculusQ(u),False,If(Equal(Length(u),C1),EasyDQ(Part(u,C1),x),If(Or(BinomialQ(u,x),ProductOfLinearPowersQ(u,x)),True,If(And(RationalFunctionQ(u,x),SameQ(RationalFunctionExponents(u,x),List(C1,C1))),True,If(ProductQ(u),If(FreeQ(First(u),x),EasyDQ(Rest(u),x),If(FreeQ(Rest(u),x),EasyDQ(First(u),x),False)),If(SumQ(u),And(EasyDQ(First(u),x),EasyDQ(Rest(u),x)),If(Equal(Length(u),C2),If(FreeQ(Part(u,C1),x),EasyDQ(Part(u,C2),x),If(FreeQ(Part(u,C2),x),EasyDQ(Part(u,C1),x),False)),False))))))))),
ISetDelayed(ProductOfLinearPowersQ(u_,x_Symbol),
    Or(Or(FreeQ(u,x),MatchQ(u,Condition(Power(v_,n_DEFAULT),And(LinearQ(v,x),FreeQ(n,x))))),And(And(ProductQ(u),ProductOfLinearPowersQ(First(u),x)),ProductOfLinearPowersQ(Rest(u),x)))),
ISetDelayed(Rt(u_,$p(n, IntegerHead)),
    RtAux(TogetherSimplify(u),n)),
ISetDelayed(RtAux(Complex(a_,b_),n_),
    Condition(Power(RtAux(Plus(Times(a,Power(Plus(Sqr(a),Sqr(b)),CN1)),Times(CN1,b,Power(Plus(Sqr(a),Sqr(b)),CN1),CI)),n),CN1),And(And(And(RationalQ(a,b),Or(Not(IntegerQ(a)),Not(IntegerQ(b)))),IntegerQ(Times(a,Power(Plus(Sqr(a),Sqr(b)),CN1)))),IntegerQ(Times(b,Power(Plus(Sqr(a),Sqr(b)),CN1)))))),
ISetDelayed(RtAux(Times(CN1,Power(v_,m_)),n_),
    Condition(Power(RtAux(Times(CN1,v),n),m),And(And(OddQ(m),SumQ(v)),Less(NumericFactor(Part(v,C1)),C0)))),
ISetDelayed(RtAux(Times(u_DEFAULT,Power(v_,m_DEFAULT),Power(w_,p_DEFAULT)),n_),
    Condition(Times(RtAux(u,n),Power(RtAux(Times(CN1,v),n),m),Power(RtAux(Times(CN1,w),n),p)),And(And(And(And(And(And(OddQ(m),OddQ(p)),SumQ(v)),SumQ(w)),Less(NumericFactor(Part(v,C1)),C0)),SumQ(w)),Less(NumericFactor(Part(w,C1)),C0)))),
ISetDelayed(RtAux(Times(CN1,u_,Power(v_,m_DEFAULT)),n_),
    Condition(Times(RtAux(u,n),RtAux(Times(CN1,Power(v,m)),n)),And(And(And(EvenQ(n),OddQ(m)),SumQ(v)),Less(NumericFactor(Part(v,C1)),C0)))),
ISetDelayed(RtAux(Power(u_,m_),n_),
    Condition(Power(RtAux(Power(u,Times(CN1,m)),n),CN1),And(RationalQ(m),Less(m,C0)))),
ISetDelayed(RtAux(Times(CN1,Power(u_,m_)),n_),
    Condition(Power(RtAux(Times(CN1,Power(u,Times(CN1,m))),n),CN1),And(RationalQ(m),Less(m,C0)))),
ISetDelayed(RtAux(Times(v_DEFAULT,Power(u_,w_)),n_),
    Condition(Module(List(Set(m,Numerator(NumericFactor(w)))),Condition(Times(RtAux(v,n),Power(RtAux(Power(u,Times(w,Power(m,CN1))),Times(n,Power(GCD(m,n),CN1))),Times(m,Power(GCD(m,n),CN1)))),Greater(m,C1))),Not(NegativeOrZeroQ(v)))),
ISetDelayed(RtAux(u_,n_),
    Condition(Module(List(i),Catch(CompoundExpression(CompoundExpression(Do(If(PositiveQ(Part(u,i)),Throw(Times(RtAux(Part(u,i),n),RtAux(Delete(u,i),n)))),List(i,C1,Length(u))),Do(If(And(NegativeQ(Part(u,i)),NonzeroQ(Plus(Part(u,i),C1))),Throw(Times(RtAux(Times(CN1,Part(u,i)),n),RtAux(Times(CN1,Delete(u,i)),n)))),List(i,C1,Length(u)))),If(SameQ(Part(u,C1),CN1),CompoundExpression(CompoundExpression(CompoundExpression(Do(If(And(SumQ(Part(u,i)),Or(NegQ(Part(u,i,C1)),NegQ(Part(u,i,C2)))),Throw(Times(RtAux(Plus(Times(CN1,First(Part(u,i))),Times(CN1,Rest(Part(u,i)))),n),RtAux(Times(CN1,Delete(u,i)),n)))),List(i,C2,Length(u))),Do(If(And(And(And(PowerQ(Part(u,i)),OddQ(Part(u,i,C2))),SumQ(Part(u,i,C1))),Or(NegQ(Part(u,i,C1,C1)),NegQ(Part(u,i,C1,C2)))),Throw(Times(RtAux(Power(Plus(Times(CN1,First(Part(u,i,C1))),Times(CN1,Rest(Part(u,i,C1)))),Part(u,i,C2)),n),RtAux(Times(CN1,Delete(u,i)),n)))),List(i,C2,Length(u)))),Do(If(Or(AtomQ(Part(u,i)),And(And(PowerQ(Part(u,i)),OddQ(Part(u,i,C2))),AtomQ(Part(u,i,C1)))),Throw(Times(RtAux(Times(CN1,Part(u,i)),n),RtAux(Times(CN1,Delete(u,i)),n)))),List(i,C2,Length(u)))),Times(RtAux(Times(CN1,Part(u,C2)),n),RtAux(Drop(u,C2),n))),CompoundExpression(Do(If(Not(FreeQ(Delete(u,i),RtAux(Times(CN1,Part(u,i)),n))),Throw(Times(RtAux(Times(CN1,Part(u,i)),n),RtAux(Times(CN1,Delete(u,i)),n)))),List(i,C1,Length(u))),Map(Function(RtAux(Slot1,n)),u)))))),And(And(ProductQ(u),EvenQ(n)),Not(And(SameQ(Part(u,C1),CN1),Equal(Length(u),C2)))))),
ISetDelayed(RtAux(u_,n_),
    Condition(Map(Function(RtAux(Slot1,n)),u),And(OddQ(n),ProductQ(u)))),
ISetDelayed(RtAux(u_,n_),
    Condition(Times(CN1,RtAux(Times(CN1,u),n)),And(OddQ(n),NegativeQ(u)))),
ISetDelayed(RtAux(u_,n_),
    Power(u,Power(n,CN1))),
ISetDelayed(IntSum(u_,x_Symbol),
    Plus(Simp(Times(FreeTerms(u,x),x),x),IntTerm(NonfreeTerms(u,x),x))),
ISetDelayed(IntTerm(Times(c_DEFAULT,Power(v_,CN1)),x_Symbol),
    Condition(Simp(Times(c,Log(RemoveContent(v,x)),Power(Coefficient(v,x,C1),CN1)),x),And(FreeQ(c,x),LinearQ(v,x)))),
ISetDelayed(IntTerm(Times(c_DEFAULT,Power(v_,m_DEFAULT)),x_Symbol),
    Condition(Simp(Times(c,Power(v,Plus(m,C1)),Power(Times(Coefficient(v,x,C1),Plus(m,C1)),CN1)),x),And(And(FreeQ(List(c,m),x),NonzeroQ(Plus(m,C1))),LinearQ(v,x)))),
ISetDelayed(IntTerm(u_,x_Symbol),
    Condition(Map(Function(IntTerm(Slot1,x)),u),SumQ(u))),
ISetDelayed(IntTerm(u_,x_Symbol),
    Dist(FreeFactors(u,x),Int(NonfreeFactors(u,x),x),x)),
ISetDelayed(SimplerQ(u_,v_),
    If(IntegerQ(u),If(IntegerQ(v),If(Equal(Abs(u),Abs(v)),Less(v,C0),Less(Abs(u),Abs(v))),True),If(IntegerQ(v),False,If(SameQ(Head(u),$s("Rational")),If(SameQ(Head(v),$s("Rational")),If(Equal(Denominator(u),Denominator(v)),SimplerQ(Numerator(u),Numerator(v)),Less(Denominator(u),Denominator(v))),True),If(SameQ(Head(v),$s("Rational")),False,If(SameQ(Head(u),$s("Complex")),If(SameQ(Head(v),$s("Complex")),If(Equal(Re(u),Re(v)),SimplerQ(Im(u),Im(v)),SimplerQ(Re(u),Re(v))),False),If(NumberQ(u),If(NumberQ(v),OrderedQ(List(u,v)),True),If(NumberQ(v),False,If(AtomQ(u),If(AtomQ(v),OrderedQ(List(u,v)),True),If(AtomQ(v),False,If(SameQ(Head(u),Head(v)),If(Equal(Length(u),Length(v)),Catch(CompoundExpression(Do(If(SameQ(Part(u,$s("ii")),Part(v,$s("ii"))),Null,Throw(SimplerQ(Part(u,$s("ii")),Part(v,$s("ii"))))),List($s("ii"),Length(u))),False)),Less(Length(u),Length(v))),Less(LeafCount(u),LeafCount(v))))))))))))),
ISetDelayed(SimplerIntegrandQ(u_,v_,x_Symbol),
    Module(List(Set($s("lst"),CancelCommonFactors(u,v)),$s("u1"),$s("v1")),CompoundExpression(CompoundExpression(Set($s("u1"),Part($s("lst"),C1)),Set($s("v1"),Part($s("lst"),C2))),If(Less(LeafCount($s("u1")),Times(QQ(3L,4L),LeafCount($s("v1")))),True,If(RationalFunctionQ($s("u1"),x),If(RationalFunctionQ($s("v1"),x),Less(Apply(Plus,RationalFunctionExponents($s("u1"),x)),Apply(Plus,RationalFunctionExponents($s("v1"),x))),True),False))))),
ISetDelayed(CancelCommonFactors(u_,v_),
    If(ProductQ(u),If(ProductQ(v),If(MemberQ(v,First(u)),CancelCommonFactors(Rest(u),DeleteCases(v,First(u),C1,C1)),$(Function(List(Times(First(u),Part(Slot1,C1)),Part(Slot1,C2))),CancelCommonFactors(Rest(u),v))),If(MemberQ(u,v),List(DeleteCases(u,v,C1,C1),C1),List(u,v))),If(ProductQ(v),If(MemberQ(v,u),List(C1,DeleteCases(v,u,C1,C1)),List(u,v)),List(u,v)))),
ISetDelayed(SumSimplerQ(u_,v_),
    If(RationalQ(u,v),If(Equal(v,C0),False,If(Greater(v,C0),Less(u,CN1),GreaterEqual(u,Times(CN1,v)))),SumSimplerAuxQ(Expand(u),Expand(v)))),
ISetDelayed(SumSimplerAuxQ(u_,v_),
    Condition(And(Or(RationalQ(First(v)),SumSimplerAuxQ(u,First(v))),Or(RationalQ(Rest(v)),SumSimplerAuxQ(u,Rest(v)))),SumQ(v))),
ISetDelayed(SumSimplerAuxQ(u_,v_),
    Condition(Or(SumSimplerAuxQ(First(u),v),SumSimplerAuxQ(Rest(u),v)),SumQ(u))),
ISetDelayed(SumSimplerAuxQ(u_,v_),
    And(And(UnsameQ(v,C0),SameQ(NonnumericFactors(u),NonnumericFactors(v))),Or(Less(Times(NumericFactor(u),Power(NumericFactor(v),CN1)),CN1D2),And(Equal(Times(NumericFactor(u),Power(NumericFactor(v),CN1)),CN1D2),Less(NumericFactor(u),C0))))),
ISetDelayed(SimplerSqrtQ(u_,v_),
    Module(List(Set($s("sqrtu"),Rt(u,C2)),Set($s("sqrtv"),Rt(v,C2))),If(IntegerQ($s("sqrtu")),If(IntegerQ($s("sqrtv")),Less($s("sqrtu"),$s("sqrtv")),True),If(IntegerQ($s("sqrtv")),False,If(RationalQ($s("sqrtu")),If(RationalQ($s("sqrtv")),Less($s("sqrtu"),$s("sqrtv")),True),If(RationalQ($s("sqrtv")),False,If(PosQ(u),If(PosQ(v),Less(LeafCount($s("sqrtu")),LeafCount($s("sqrtv"))),True),If(PosQ(v),False,Less(LeafCount($s("sqrtu")),LeafCount($s("sqrtv"))))))))))),
ISetDelayed(FixIntRule(RuleDelayed($p("lhs"),$($p("F"),$($p("G"),$p("§list"),$($p("F"),Plus(u_,v_),$p("test2"))),$p("test1"))),x_),
    Condition(ReplacePart(RuleDelayed($s("lhs"),F(G($s("§list"),F(Plus(u,v),$s("test2"))),$s("test1"))),List(Rule(List(C2,C1,C2,C1,C1),FixRhsIntRule(u,x)),Rule(List(C2,C1,C2,C1,C2),FixRhsIntRule(v,x)))),And(SameQ($s("F"),$s("Condition")),Or(SameQ($s("G"),$s("Module")),SameQ($s("G"),$s("Block")))))),
ISetDelayed(FixIntRule(RuleDelayed($p("lhs"),$($p("G"),$p("§list"),$($p("F"),Plus(u_,v_),$p("test2")))),x_),
    Condition(ReplacePart(RuleDelayed($s("lhs"),G($s("§list"),F(Plus(u,v),$s("test2")))),List(Rule(List(C2,C2,C1,C1),FixRhsIntRule(u,x)),Rule(List(C2,C2,C1,C2),FixRhsIntRule(v,x)))),And(SameQ($s("F"),$s("Condition")),Or(SameQ($s("G"),$s("Module")),SameQ($s("G"),$s("Block")))))),
ISetDelayed(FixIntRule(RuleDelayed($p("lhs"),$($p("F"),$($p("G"),$p("§list"),Plus(u_,v_)),$p("test"))),x_),
    Condition(ReplacePart(RuleDelayed($s("lhs"),F(G($s("§list"),Plus(u,v)),$s("test"))),List(Rule(List(C2,C1,C2,C1),FixRhsIntRule(u,x)),Rule(List(C2,C1,C2,C2),FixRhsIntRule(v,x)))),And(SameQ($s("F"),$s("Condition")),Or(SameQ($s("G"),$s("Module")),SameQ($s("G"),$s("Block")))))),
ISetDelayed(FixIntRule(RuleDelayed($p("lhs"),$($p("G"),$p("§list"),Plus(u_,v_))),x_),
    Condition(ReplacePart(RuleDelayed($s("lhs"),G($s("§list"),Plus(u,v))),List(Rule(List(C2,C2,C1),FixRhsIntRule(u,x)),Rule(List(C2,C2,C2),FixRhsIntRule(v,x)))),Or(SameQ($s("G"),$s("Module")),SameQ($s("G"),$s("Block"))))),
ISetDelayed(FixIntRule(RuleDelayed($p("lhs"),$($p("F"),Plus(u_,v_),$p("test"))),x_),
    Condition(ReplacePart(RuleDelayed($s("lhs"),F(Plus(u,v),$s("test"))),List(Rule(List(C2,C1,C1),FixRhsIntRule(u,x)),Rule(List(C2,C1,C2),FixRhsIntRule(v,x)))),SameQ($s("F"),$s("Condition")))),
ISetDelayed(FixIntRule(RuleDelayed($p("lhs"),Plus(u_,v_)),x_),
    ReplacePart(RuleDelayed($s("lhs"),Plus(u,v)),List(Rule(List(C2,C1),FixRhsIntRule(u,x)),Rule(List(C2,C2),FixRhsIntRule(v,x))))),
ISetDelayed(FixIntRule(RuleDelayed($p("lhs"),$($p("F"),$($p("G"),$p("list1"),$($p("F"),$($p("H"),$p("list2"),u_),$p("test2"))),$p("test1"))),x_),
    Condition(ReplacePart(RuleDelayed($s("lhs"),F(G($s("list1"),F(H($s("list2"),u),$s("test2"))),$s("test1"))),Rule(List(C2,C1,C2,C1,C2),FixRhsIntRule(u,x))),And(And(SameQ($s("F"),$s("Condition")),Or(SameQ($s("G"),$s("Module")),SameQ($s("G"),$s("Block")))),Or(SameQ($s("H"),$s("Module")),SameQ($s("H"),$s("Block")))))),
ISetDelayed(FixIntRule(RuleDelayed($p("lhs"),$($p("F"),$($p("G"),$p("§list"),$($p("F"),$($p("H"),$p("str1"),$p("str2"),$p("str3"),$($p("jp"),u_)),$p("test2"))),$p("test1"))),x_),
    Condition(ReplacePart(RuleDelayed($s("lhs"),F(G($s("§list"),F(H($s("str1"),$s("str2"),$s("str3"),$($s("jp"),u)),$s("test2"))),$s("test1"))),Rule(List(C2,C1,C2,C1,C4,C1),FixRhsIntRule(u,x))),And(And(And(SameQ($s("F"),$s("Condition")),Or(SameQ($s("G"),$s("Module")),SameQ($s("G"),$s("Block")))),SameQ($s("H"),$s("§showstep"))),SameQ($s("jp"),$s("Hold"))))),
ISetDelayed(FixIntRule(RuleDelayed($p("lhs"),$($p("F"),$($p("G"),$p("§list"),$($p("F"),u_,$p("test2"))),$p("test1"))),x_),
    Condition(ReplacePart(RuleDelayed($s("lhs"),F(G($s("§list"),F(u,$s("test2"))),$s("test1"))),Rule(List(C2,C1,C2,C1),FixRhsIntRule(u,x))),And(SameQ($s("F"),$s("Condition")),Or(SameQ($s("G"),$s("Module")),SameQ($s("G"),$s("Block")))))),
ISetDelayed(FixIntRule(RuleDelayed($p("lhs"),$($p("G"),$p("§list"),$($p("F"),u_,$p("test2")))),x_),
    Condition(ReplacePart(RuleDelayed($s("lhs"),G($s("§list"),F(u,$s("test2")))),Rule(List(C2,C2,C1),FixRhsIntRule(u,x))),And(SameQ($s("F"),$s("Condition")),Or(SameQ($s("G"),$s("Module")),SameQ($s("G"),$s("Block")))))),
ISetDelayed(FixIntRule(RuleDelayed($p("lhs"),$($p("F"),$($p("G"),$p("§list"),u_),$p("test"))),x_),
    Condition(ReplacePart(RuleDelayed($s("lhs"),F(G($s("§list"),u),$s("test"))),Rule(List(C2,C1,C2),FixRhsIntRule(u,x))),And(SameQ($s("F"),$s("Condition")),Or(SameQ($s("G"),$s("Module")),SameQ($s("G"),$s("Block")))))),
ISetDelayed(FixIntRule(RuleDelayed($p("lhs"),$($p("G"),$p("§list"),u_)),x_),
    Condition(ReplacePart(RuleDelayed($s("lhs"),G($s("§list"),u)),Rule(List(C2,C2),FixRhsIntRule(u,x))),Or(SameQ($s("G"),$s("Module")),SameQ($s("G"),$s("Block"))))),
ISetDelayed(FixIntRule(RuleDelayed($p("lhs"),$($p("F"),u_,$p("test"))),x_),
    Condition(ReplacePart(RuleDelayed($s("lhs"),F(u,$s("test"))),Rule(List(C2,C1),FixRhsIntRule(u,x))),SameQ($s("F"),$s("Condition")))),
ISetDelayed(FixIntRule(RuleDelayed($p("lhs"),u_),x_),
    ReplacePart(RuleDelayed($s("lhs"),u),Rule(List(C2),FixRhsIntRule(u,x)))),
ISetDelayed(FixRhsIntRule(Plus(u_,Times(CN1,v_)),x_),
    Plus(FixRhsIntRule(u,x),Times(CN1,FixRhsIntRule(v,x)))),
ISetDelayed(FixRhsIntRule(Times(CN1,u_),x_),
    Times(CN1,FixRhsIntRule(u,x))),
ISetDelayed(FixRhsIntRule(Times(a_,u_),x_),
    Condition(Dist(a,u,x),MemberQ(List($s("Int"),$s("Integrate::Subst")),Head(Unevaluated(u))))),
ISetDelayed(FixRhsIntRule(u_,x_),
    If(And(SameQ(Head(Unevaluated(u)),$s("Integrate::Dist")),Equal(Length(Unevaluated(u)),C2)),Insert(Unevaluated(u),x,C3),If(MemberQ(List($s("Int"),$s("Integrate::Subst"),Defer($s("Int")),$s("Integrate::Simp"),$s("Integrate::Dist")),Head(Unevaluated(u))),u,Simp(u,x))))
  );
}
